import React from 'react';

class AppointmentsForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: '',
            owner: '',
            appointmentDate: '',
            reason: '',
            technicians: [],
        };
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleAppointmentDateChange = this.handleAppointmentDateChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);
        this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.appointment_date = data.appointmentDate;
        delete data.appointmentDate;
        delete data.technicians;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
          const newAppointment = await response.json();
          console.log(newAppointment);
          const cleared = {
            vin: '',
            owner: '',
            appointmentDate: '',
            reason: '',
            technician: '',
          };
          this.setState(cleared);
        }
    }

    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value});
      }

    handleOwnerChange(event) {
        const value = event.target.value;
        this.setState({owner: value});
    }

    handleAppointmentDateChange(event) {
        const value = event.target.value;
        this.setState({appointmentDate: value});
    }

    handleReasonChange(event) {
        const value = event.target.value;
        this.setState({reason: value});
    }

    handleTechnicianChange(event) {
        const value = event.target.value;
        this.setState({technician: value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({technicians: data.technicians});


          }
        }


    render() {
    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new appointment</h1>
              <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" value={this.state.vin} className="form-control" />
                  <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleOwnerChange} placeholder="Owner" required type="text" name="owner" id="owner" value={this.state.owner} className="form-control"/>
                <label htmlFor="owner">Owner</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleAppointmentDateChange} placeholder="Appointment date" required type="datetime-local" name="appointmentDate" id="appointmentDate" value={this.state.appointmentDate} className="form-control"/>
                <label htmlFor="appointmentDate">Appointment Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" value={this.state.reason} className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleTechnicianChange} required name="technician" id="technician" value={this.state.technician} className="form-select">
                  <option value="">Choose a technician</option>
                  {this.state.technicians.map(technician => {
                    return (
                        <option key={technician.employee_number} value={technician.employee_number}>
                        {technician.name}
                  </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentsForm;