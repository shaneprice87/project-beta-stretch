import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './sales/SalesPersonForm';
import CustomerForm from './sales/CustomerForm';
import SaleRecordForm from './sales/SaleRecordForm';
import SalesRecordList from './sales/SaleRecordsList';
import AppointmentsList from './service/AppointmentsList';
import SalesPersonRecordList from './sales/SalesPersonRecordList';

import AppointmentsForm from './service/AppointmentsForm';
import TechnicianForm from './service/TechnicianForm';
import ManufacturersList from './inventory/ManufacturersList';
import ModelsList from './inventory/VehicleModelsList';
import AutomobilesList from './inventory/AutomobilesList';
import ManufacturerForm from './inventory/ManufacturersForm';
import ModelForm from './inventory/VehicleModelsForm';
import AutomobileForm from './inventory/AutomobilesForm';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salesperson" element={<SalesPersonForm />} />
          <Route path="/customer" element={<CustomerForm />} />
          <Route path="/salesrecord" element={<SaleRecordForm />} />
          <Route path="/saleslist" element={<SalesRecordList />} />

          <Route path="/salespersonlist" element={<SalesPersonRecordList />} />

          <Route path="/appointments" element={<AppointmentsList />} />
          <Route path="/appointments/new" element={<AppointmentsForm />} />

          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelsList />} />
          <Route path="/models/new" element={<ModelForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
