import React from 'react';
import { Link } from 'react-router-dom';

class ModelForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            picture_url: '',
            manufacturers: [],
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/manufacturers/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                manufacturers: data.manufacturers
            });
        }
    }


    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.manufacturer_id = data.manufacturer
        delete data.manufacturers;

        const response = await fetch ('http://localhost:8100/api/models/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });

    
      if (response.ok) {
        const cleared = {
          manufacturers: [],
          picture_url: '',
          name: '',
        };
        this.setState(cleared);
      }
    }
    
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Vehicle Model</h1>
              <form onSubmit={this.handleSubmit} id="create-model-form">
                <div className="mb-3">
                  <select onChange={this.handleChange} required name="manufacturer" id="manufacturer" className="form-select">
                  <option value="">Choose a Manufacturer</option>
                    {this.state.manufacturers.map(manufacturer => {
                      return (
                      <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                  </select>
                </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div> 
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div> 
              <button className="btn bg-success">Add</button>
            </form>
          </div>
        </div>
      </div>
    )
    }


}

export default ModelForm
