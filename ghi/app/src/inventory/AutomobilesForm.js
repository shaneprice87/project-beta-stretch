import React from 'react';
import { Link } from 'react-router-dom';

class AutomobileForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            models: [],
            vin: '',
            color: '',
            year: '',
            model: ''
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/models/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                models: data.models
            });
        }
    }


    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.model_id = data.model
        delete data.model
        delete data.models;

        const response = await fetch ('http://localhost:8100/api/automobiles/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });

    
      if (response.ok) {
        const cleared = {
          models: [],
          vin: '',
          color: '',
          year: ''
        };
        this.setState(cleared);
      }
    }
    
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add Automobile</h1>
              <form onSubmit={this.handleSubmit} id="create-model-form">
                <div className="mb-3">
                  <select onChange={this.handleChange} required name="model" id="model" className="form-select">
                  <option value="">Make and Model</option>
                    {this.state.models.map(model => {
                      return (
                      <option key={model.id} value={model.id}>{model.manufacturer.name} {model.name}</option>
                      )
                    })}
                  </select>
                </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div> 
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div> 
              <div className="form-floating mb-3">
                <input onChange={this.handleChange} value={this.state.year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div> 
              <button className="btn bg-success">Add</button>
            </form>
          </div>
        </div>
      </div>
    )
    }


}

export default AutomobileForm