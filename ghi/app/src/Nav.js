import { NavLink } from 'react-router-dom';



function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Sales</button>
                <ul className="dropdown-menu">
                  <li className="nav-item"><NavLink className="dropdown-item" to="/salesperson">Create Salesperson</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/customer">Create Customer</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/salesrecord">Create Sale Record</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/saleslist">All Sales Records</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/salespersonlist">Sales Person Records</NavLink></li>
                </ul>
            </div>
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Service</button>
                <ul className="dropdown-menu">
                  <li className="nav-item"><NavLink className="dropdown-item" to="/technicians/new">Create Technician</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/appointments/new">Create Appointment</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/appointments">Appointments</NavLink></li>
                </ul>
            </div>
            <div className="dropdown">
              <button className="btn dropdown-toggle" data-bs-toggle="dropdown">Inventory</button>
                <ul className="dropdown-menu">
                  <li className="nav-item"><NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/manufacturers/new">Add Manufacturers</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/models">Vehicle Models</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/models/new">Add Vehicle Models</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/automobiles">Automobiles in inventory</NavLink></li>
                  <li className="nav-item"><NavLink className="dropdown-item" to="/automobiles/new">Add Automobile to inventory</NavLink></li>
                </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
