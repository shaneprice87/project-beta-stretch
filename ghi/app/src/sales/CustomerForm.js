import React from 'react';
import { Link } from 'react-router-dom';

class CustomerForm extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          address: '',
          phoneNumber: '',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangePhoneNumber = this.handleChangePhoneNumber.bind(this);
      }
    
    
      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.phone_number = data.phoneNumber;
        delete data.phoneNumber;

    
        const customerUrl = `http://localhost:8090/api/sales/customers/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
          const newCustomer = await response.json();

          const cleared = {
            name: '',
            address: '',
            phoneNumber: ''
          };
          this.setState(cleared);
        }
      }
    
      handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
      }
    
      handleChangeAddress(event) {
        const value = event.target.value;
        this.setState({ address: value });
      }

      handleChangePhoneNumber(event) {
        const value = event.target.value;
        this.setState({ phoneNumber: value });
      }
      

    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Customer</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">

                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeName} value={this.state.name} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>

                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangeAddress} value={this.state.address} placeholder="Customer Address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="address">Customer Address</label>
                  </div>

                  <div className="form-floating mb-3">
                    <input onChange={this.handleChangePhoneNumber} value={this.state.phoneNumber} placeholder="Customer Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="phone_number">Customer Phone Number</label>
                  </div>

                  <button className="btn bg-success">Create</button>
                </form>
              </div>
            </div>
          </div>
        )

    }
}

export default CustomerForm