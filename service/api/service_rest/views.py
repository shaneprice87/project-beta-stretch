from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "owner",
        "appointment_date",
        "technician",
        "reason",
        "is_completed",
        "was_canceled",
        "pk",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        print(count)

        if count > 0:
            return {"vin_is_from_inventory": True}

        else:
            return {"vin_is_from_inventory": False}



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    vin = request.GET.get("vin")

    if request.method == "GET":
        if vin is not None:
            appointments = Appointment.objects.filter(vin=vin)
        else:
            appointments = Appointment.objects.filter(is_completed=False, was_canceled=False)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            technician = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request, employee_number=None):
    if request.method == "GET":
        if employee_number is not None:
            technicians = Technician.objects.filter(employee_number=employee_number)
        else:
            technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT", "DELETE"])
def api_complete_appointment(request, pk):
    if request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
