from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return str(self.vin)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    owner = models.CharField(max_length=100)
    appointment_date = models.DateTimeField()
    reason = models.CharField(max_length=200, null=True)

    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    vin_is_from_inventory = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    was_canceled = models.BooleanField(default=False)

    def __str__(self):
        return self.owner + " on " + str(self.appointment_date) + " for " + str(self.reason)
