
from common.json import ModelEncoder
from .models import AutomobileVO , SalesPerson , PotentialCustomer , SalesRecord , AutoDetailsVO


class AutoDetailsEncoder(ModelEncoder):
    model = AutoDetailsVO
    properties = ["vin", "make", "model"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin", "id"]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name", "address", "phone_number", "id"]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["price", "automobile", "sales_person", "customer", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder()
    }
